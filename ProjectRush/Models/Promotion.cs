﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.AspNet.Identity.EntityFramework;


namespace ProjectRush.Models
{
    public class Promotion: Entity
    {
        #region "Accessor properties"
        public string Title { get; set; }
        public string Description { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string CreatorId { get; set; }

        [ForeignKey("CreatorId")]
        public virtual IdentityUser Creator { get; set; }

        #endregion
    }

    
}