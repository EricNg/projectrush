﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace ProjectRush.Models
{
    public interface IEntity
    {
        #region "Accessor properties"

        int Id { get; set; }
        string UniqueKey { get; set; }

        #endregion
    }

    public abstract class Entity : IEntity
    {
        private string _key;

        #region "Accessor properties"

        [Key]
        public int Id { get; set; }

        [Required]
        [ScaffoldColumn(false)]
        [StringLength(50, MinimumLength = 19)]
        public virtual string UniqueKey
        {
            get { return _key = _key ?? Guid.NewGuid().ToString(); }
            set { _key = value; }
        }

        #endregion
    }
}