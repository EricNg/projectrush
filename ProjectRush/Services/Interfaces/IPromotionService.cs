﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ProjectRush.Models;

namespace ProjectRush.Services
{
    public interface IPromotionService
    {
        IQueryable<Promotion> GetPromotions();
        Promotion GetUserByPromotionId(int id);
        void InsertPromotion(Promotion promo);
        Promotion GetPromotionById(int id);
        void DeletePromotion(int id);
        void UpdatePromotion(Promotion promo);
        void Save();

    }
}