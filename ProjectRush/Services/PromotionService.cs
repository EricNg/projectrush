﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ProjectRush.Models;
using System.Data.Entity;


namespace ProjectRush.Services
{
    public class PromotionService : BaseService, IPromotionService
    {
        public IQueryable<Promotion> GetPromotions()
        {
            return _db.Promotions;
        }
     
        public void InsertPromotion(Promotion promo)
        {
            _db.Promotions.Add(promo);

        }

        public Promotion GetPromotionById(int id)
        {
            return _db.Promotions.Find(id);
        }

        public void DeletePromotion(int id)
        {
            Promotion promo = _db.Promotions.Find(id);
            _db.Promotions.Remove(promo);
        }

        public void UpdatePromotion(Promotion promo)
        {
            _db.Entry(promo).State = EntityState.Modified;

        }

        public void Save()
        {
            _db.SaveChanges();
        }
    }
}