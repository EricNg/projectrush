﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ProjectRush.Models;

namespace ProjectRush.Services
{
    public class BaseService
    {
        protected ApplicationDbContext _db;

        public BaseService()
        {
            _db = new ApplicationDbContext();
        }
    }
}